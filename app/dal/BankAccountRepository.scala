package dal

import models.{Deposit, WithDraw}

/**
  * Created by hungai on 11/22/16.
  */
object BankAccountRepository {

  // Implemented the DataStore using the LinkedList Data Structure

  var deposits = List[Deposit]()

  var withdrawals = List[WithDraw]()


  def addDeposit(amount:Double):Deposit = {
    val statement = "Successful Deposit"
    val newDeposit = Deposit(amount,statement)
      deposits = deposits ::: List(newDeposit)
    newDeposit
  }

  def addWithdrawal(amount:Double):WithDraw = {
    val statement = "Successful Withdrawal"
    val newWithDraw = WithDraw(amount,statement)
      withdrawals = withdrawals ::: List(newWithDraw)
    newWithDraw
  }

  def totalWithdrawals = withdrawals.foldLeft(0.0)((r,c) => r + c.amount)
  def totalDeposits = deposits.foldLeft(0.0)((r,c) => r + c.amount)

  def depositFrequency = deposits.length
  def withdrawalFrequency = withdrawals.length

  def outStandingBalance = totalDeposits - totalWithdrawals

}
