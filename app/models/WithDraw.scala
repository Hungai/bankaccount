package models

import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._

/**
  * Created by hungai on 11/22/16.
  */
case class WithDraw(amount:Double,statement:String)

object WithDraw {

  implicit val withdrawWrites:Writes[WithDraw] = (
    (JsPath \ "amount").write[Double] and
      (JsPath \ "statement").write[String]
    )(unlift(WithDraw.unapply))

  implicit val withdrawReads:Reads[WithDraw] = (
    (JsPath \ "amount").read[Double] (min(0.0) keepAnd max(20000.0)) and
      (JsPath \ "statement").read[String]
    )(WithDraw.apply _)

}





/**
  * sealed trait Transaction {
  *   def amount:Double
  *   ef statement:String
  * }
  * case class Deposit(amount:Double,statement:String) extends Transaction
  * case class Withdraw(amount:Double,statement:String) extends Transaction
  *
  */