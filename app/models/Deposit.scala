package models

import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._

/**
  * Created by hungai on 11/22/16.
  */


//Model for a Deposit

case class Deposit (amount:Double, statement:String)

object Deposit {
  implicit val depositWrites:Writes[Deposit] = (
    (JsPath \ "amount").write[Double] and
      (JsPath\ "statement").write[String]
    )(unlift(Deposit.unapply))

  implicit val depositReads:Reads[Deposit] = (
    (JsPath \ "amount").read[Double] (min(0.0) keepAnd max(40000.0)) and
      (JsPath \ "statement").read[String]
    )(Deposit.apply _)
}



/**
  * sealed trait Transaction {
  *   def amount:Double
  *   ef statement:String
  * }
  * case class Deposit(amount:Double,statement:String) extends Transaction
  * case class Withdraw(amount:Double,statement:String) extends Transaction
  *
  */