package controllers

import dal.BankAccountRepository
import play.api.libs.json._
import models.Deposit._
import models.WithDraw._
import dal.BankAccountRepository._
import play.api.mvc._


/**
  * Created by hungai on 11/22/16.
  */


class Banking extends Controller {

  def balance = Action { request =>
    Ok(Json.prettyPrint(Json.obj("balance" -> BankAccountRepository.outStandingBalance))).as("application/json")
  }


  //Deposit Action

  def deposit(money:Double) = Action { request =>
    if(BankAccountRepository.depositFrequency >= 4) {
      BadRequest(Json.prettyPrint(Json.obj(
        "message" -> s"Exceeded Maximum Frequency for Today's Deposits $depositFrequency"
      )))
    } else if (BankAccountRepository.totalDeposits > 140999) {
      BadRequest(Json.prettyPrint(Json.obj(
        "message" -> s"Exceeded Maximum Daily Deposit $totalDeposits"
      )))
    } else if (money > 39999) {
      BadRequest(Json.prettyPrint(Json.obj(
        "message" -> "Exceeded maximum Deposit Per Transaction (40,000)"
      )))

    } else {
      val depo = BankAccountRepository.addDeposit(money)
      Status(201) (Json.prettyPrint(Json.toJson(depo)
      ))
    }
  }


  //Withdraw Action

  def withdraw(money:Double) = Action { request =>
    if(BankAccountRepository.withdrawalFrequency >= 3) {
      BadRequest(Json.prettyPrint(Json.obj(
        "message" -> s"Exceeded Maximum Withdrawal Frequency $withdrawalFrequency"
      )))
    } else if (BankAccountRepository.totalWithdrawals > 49999) {
      BadRequest(Json.prettyPrint(Json.obj(
        "message " -> s"Exceeded Maximum Daily WithDrawal $totalWithdrawals"
      )))
    } else if (money > 19999) {
      BadRequest(Json.prettyPrint(Json.obj(
        "message" -> "Exceeded Maximum Withdrawal Per Transaction (20,000)"
      )))
    } else if (money > BankAccountRepository.outStandingBalance) {
      BadRequest(Json.prettyPrint(Json.obj(
        "message" -> s"Withdrawal amount $money exceeds your account balance $outStandingBalance "
      )))
    } else {
      val wt = BankAccountRepository.addWithdrawal(money)
      Status(201)(Json.prettyPrint(Json.toJson(wt)
      ))
    }
  }

  def deposits = Action {
    val dps = BankAccountRepository.deposits
    Ok(Json.prettyPrint(Json.toJson(dps))).as("application/json")
  }

  def withdrawals = Action {
    val wt = BankAccountRepository.withdrawals
    Ok(Json.prettyPrint(Json.toJson(wt))).as("application/json")
  }

}