name := "bankaccount"

version := "1.0"

lazy val `bankaccount` = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.7"

val jodaVersion = "2.9.6"



libraryDependencies ++= Seq(
  jdbc ,
  cache ,
  ws   ,
  "joda-time" % "joda-time" % jodaVersion,
  specs2 % Test )



unmanagedResourceDirectories in Test <+=  baseDirectory ( _ /"target/web/public/test" )  

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"  