package controllers

import org.specs2.specification._
import play.api.test._


/**
  * Created by hungai on 11/23/16.
  */
class PublicAPISpec extends PlaySpecification {

  "Developers" should {
    "be able to get the account balance from '/balance'" in new WithApplication {
     val Some(balance) = route(app,FakeRequest(GET,"/balance"))
      status(balance) must equalTo(OK)
      contentType(balance) must beSome("application/json")
    }
    "be able to make a deposit via '/deposit/:money endpoint'" in new WithApplication {
      val Some(deposit) = route(app,FakeRequest(POST,"/deposit/100"))
      status(deposit) must equalTo(CREATED)
    }
    "not be able to more than 150,000 per day" in new WithApplication {
      val Some(deposit) = route(app,FakeRequest(POST,"/deposit/150000"))
      status(deposit) must equalTo(BAD_REQUEST)
    }
    "not be able to deposit more than 40,000 per transaction" in new WithApplication {
      val Some(deposit) = route(app,FakeRequest(POST,"/deposit/40000"))
      status(deposit) must equalTo(BAD_REQUEST)
    }
    "not able to deposit more than 4 times a day" in new WithApplication {
      val Some(trans1) = route(app,FakeRequest(POST,"/deposit/100"))
      val Some(trans2) = route(app,FakeRequest(POST,"/deposit/100"))
      val Some(trans3) = route(app,FakeRequest(POST,"/deposit/100"))
      val Some(trans4) = route(app,FakeRequest(POST,"/deposit/100"))
      val Some(trans5) = route(app,FakeRequest(POST,"/deposit/100"))

      status(trans5) must equalTo(BAD_REQUEST)

    }
    "be able to withdraw from the account on the endpoint '/withdraw/:money'" in new WithApplication {
      val Some(trans1) = route(app,FakeRequest(POST,"/deposit/1000"))
      val Some(wt) = route(app,FakeRequest(POST,"/withdraw/100"))
      status(wt) must equalTo(CREATED)

    }
    "not be able to withdraw more than 50,000 for the day" in new WithApplication {
      val Some(trans1) = route(app,FakeRequest(POST,"/deposit/10000"))
      val Some(trans2) = route(app,FakeRequest(POST,"/deposit/10000"))
      val Some(trans3) = route(app,FakeRequest(POST,"/deposit/10000"))
      val Some(trans4) = route(app,FakeRequest(POST,"/deposit/9000"))
      val Some(wt) = route(app,FakeRequest(POST,"/withdraw/50000"))
      status(wt) must equalTo(BAD_REQUEST)
    }
    "not able to withdraw more than 20,000 per transaction" in new WithApplication {
      val Some(wt) = route(app,FakeRequest(POST,"/withdraw/20000"))
      status(wt) must equalTo(BAD_REQUEST)
    }

    "not able to withdraw  more  3 times a day" in new WithApplication {
      val Some(trans) = route(app,FakeRequest(POST,"/deposit/1000"))
      val Some(trans1) = route(app,FakeRequest(POST,"/withdraw/10"))
      val Some(trans2) = route(app,FakeRequest(POST,"/withdraw/10"))
      val Some(trans3) = route(app,FakeRequest(POST,"/withdraw/10"))
      val Some(trans4) = route(app,FakeRequest(POST,"/withdraw/90"))
      val Some(wt) = route(app,FakeRequest(POST,"/withdraw/100"))

      status(wt) must equalTo(BAD_REQUEST)
    }

    "not able to make a withdraw when the balance is less than the amount" in new WithApplication {
      val Some(trans) = route(app,FakeRequest(POST,"/deposit/100"))
      val Some(wt) = route(app,FakeRequest(POST,"/withdraw/1000"))

      status(wt) must equalTo(BAD_REQUEST)
    }

  }

}
