package controllers

import org.specs2.specification._
import play.api.mvc._
import play.api.test._
import scala.concurrent.Future


/**
  * Created by rodney on 11/23/16.
  */
class ControllersSpec extends PlaySpecification with Results {

  "The Application" should {
    "respond to the Balance Action" in new WithApplication {
      val controller = new Banking()

      val result = controller.balance() (FakeRequest())
      status(result) must equalTo(OK)
      contentType(result) must beSome("application/json")

    }

    "respond to the Withdraw Negative Action" in new WithApplication {
      val controller = new Banking()
      val result = controller.deposit(100) (FakeRequest())
      val result1 = controller.withdraw(1000) (FakeRequest())

      status(result1) must equalTo(BAD_REQUEST)

    }
  }

}
