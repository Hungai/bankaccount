package dal


import models._
import dal.BankAccountRepository._

import org.specs2.mutable.Specification

/**
  * Created by hungai on 11/23/16.
  */
class BankAccountSpecification  extends Specification {
  "Deposit, Withdraw models" should {
    "be created using an amount" in {
      val amount = 2000.0
      val dp = addDeposit(amount)
      List(dp) === List(Deposit(2000.0,"Successful Deposit"))
    }
    "be created using an amount" in {
      val amount = 200.0
      val wd = addWithdrawal(amount)
      List(wd) === List(WithDraw(200.0,"Successful Withdrawal"))
    }
  }

}
